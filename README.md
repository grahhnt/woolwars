# WoolWars
Built using Crossbow, a minigame engine written by grahhnt.

This minigame is written using the Spigot API, so any forks of Spigot will run this minigame.

This WoolWars is based on Hypixel's WoolWars.

## Features
- [x] Player management
- [x] Sidebar displays of current game state & current player scores
- [x] Games will autostart once player minimums are hit
- [x] Multiple map support

## Setup

### (Step 1) Downloading from Spigot (recommended)
[insert link]

### (Step 1) Building from source
1. Clone [Crossbow](https://gitlab.com/grahhnt/crossbow)
2. Run `mvn clean package install`
3. Clone this project `https://gitlab.com/grahhnt/woolwars.git`
4. Run `mvn clean package`
5. Copy `target/WoolWars-*.jar` to your plugins folder

### (Step 2) Plugin setup
1. Start server, then stop to generate config
2. Configure config.yml (located in `plugins/WoolWars/config.yml`)
3. Copy any other worlds to `plugins/WoolWars/maps`

## Configuring maps
1. Copy world folder to `plugins/WoolWars/maps`
   The folder should contain the level.dat and any other world files
2. Create a new section under `maps:` in the `config.yml` including the following:
```yaml
[...]

maps:
  world_identifier:      # unique identifier
    name: World Name     # name displayed to players
    author:              # who built the map; can be omitted
      name: Grant
      id: grant
    world: basic         # world folder name
    defaultSpawn:        # where players should spawn defaultly
      x: 0.5
      y: 65.0
      z: 0.5
      pitch: 0.0
      yaw: 0.0
    entities:            # entities array; see below
    - type: LAUNCHPAD
      x1: 1
      y1: 64
      z1: -6
      x2: -1
      y2: 64
      z2: -4
      power: 1.0
```

### Possible entities
#### Launchpad
Launches players that enter the region

- ID: `LAUNCHPAD`
- Is a region
- Has a power level to launch the player

##### Example
```yaml
- type: LAUNCHPAD
  x1: 1
  y1: 64
  z1: -6
  x2: -1
  y2: 64
  z2: -4
  power: 1.0
```

#### Powerup
When rounds start, powerups will spawn at these coordinates

- ID: `POWERUP`
- Single point
- Contains a group; every powerup in the same group will contain the same powerup (eg same powerup on both sides of the map)

##### Example
```yaml
- type: POWERUP
  x: 5.5
  y: 64.0
  z: 10.5
  pitch: 0.0
  yaw: 0.0
  group: '1'
```

#### Wool
The wool region in the center of the map typically

- ID: `WOOL`
- Region
- Upon round start will be filled with Snow, Wool and Quartz

##### Example
```yaml
- type: WOOL
  x1: 1
  y1: 64
  z1: 1
  x2: -1
  y2: 64
  z2: -1
```

#### Kit Selector
An NPC which includes the kit change menu

- ID: `KIT_SELECTOR`
- Single point
- Contains team; currently not used but required. Either `RED` or `BLUE`

##### Example
```yaml
- type: KIT_SELECTOR
  x: 0.5
  y: 66.0
  z: -12.5
  pitch: 180.0
  yaw: 0.0
  team: RED
```

#### Team Spawn
Where each team member will respawn at the start of each round

- ID: `TEAM_SPAWN`
- Single Point
- Contains team

##### Example
```yaml
- type: TEAM_SPAWN
  x: 0.5
  y: 66.0
  z: -15.5
  pitch: 0.0
  yaw: 0.0
  team: RED
```

#### Team Wall
The wall that prevents players from leaving starting area until round start

- ID: `TEAM_WALL`
- Region
- Contains team; current unused but required. Either `RED` or `BLUE`

#### Example
```yaml
    - type: TEAM_WALL
      x1: -3
      y1: 66
      z1: -12
      x2: 3
      y2: 68
      z2: -12
      team: RED
```

## Permissions
### `woolwars.maps.configure`
In-game maps editor; not fully completed

### `woolwars.maps.change`
Change maps with `/map` and receive map changer item upon joining

## License
This project uses an MIT license, which allows you to make a private fork of this project but retaining the license and copyright listed in `LICENSE`

I am available for hire to make a custom variant of this minigame or other plugin development, DM me on Discord @ `grahhnt#0001`