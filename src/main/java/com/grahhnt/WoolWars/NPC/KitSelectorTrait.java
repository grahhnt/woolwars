package com.grahhnt.WoolWars.NPC;

import org.bukkit.event.EventHandler;

import com.grahhnt.WoolWars.GamePlayer;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.GUIs.KitSelectorGUI;

import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.trait.TraitName;

@TraitName("kitselector")
public class KitSelectorTrait extends Trait {
	private WoolWars plugin;

	public KitSelectorTrait() {
		super("kitselector");
		plugin = WoolWars.getInstance();
	}

	@EventHandler
	public void click(NPCRightClickEvent event) {
		if (event.getNPC() != event.getNPC())
			return;
		
		GamePlayer player = plugin.game.players.getPlayer(event.getClicker());
		new KitSelectorGUI(player).open(player);
	}

	@EventHandler
	public void click(NPCLeftClickEvent event) {
		if (event.getNPC() != event.getNPC())
			return;
		
		GamePlayer player = plugin.game.players.getPlayer(event.getClicker());
		new KitSelectorGUI(player).open(player);
	}
}
