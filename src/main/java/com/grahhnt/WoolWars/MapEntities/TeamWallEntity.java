package com.grahhnt.WoolWars.MapEntities;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.Map.MapEntity.EditorDescriptor.ConfigValue;
import com.grahhnt.Crossbow.Map.RegionMapEntity;
import com.grahhnt.Crossbow.Players.CrossbowTeam;

/**
 * Describes where a team's wall spawns
 * 
 * @author grant
 *
 */
public class TeamWallEntity extends RegionMapEntity {
	private CrossbowTeam team;

	public TeamWallEntity() {
		super("TEAM_WALL");
	}

	@Override
	public Map<String, Object> serialize() {
		HashMap<String, Object> serial = super.serializeBase();
		
		serial.put("team", team.getID());
		
		return serial;
	}

	@Override
	public void deserialize(Map<String, Object> data) {
		populateRegionCoords(data);
		team = Crossbow.getInstance().players.getTeam((String) data.get("team"));
	}

	@Override
	public void spawn(World world) {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	public CrossbowTeam getTeam() {
		return team;
	}

	public void setWall(World world, Material material) {
		CrossbowUtils.fill(new Location(world, x1, y1, z1), new Location(world, x2, y2, z2), material);
	}
	
	@Override
	public EditorDescriptor getEditorDescriptor() {
		EditorDescriptor descriptor = super.getEditorDescriptor();
		descriptor.addConfigValues(new ConfigValue.TeamValue("team", team, true));
		return descriptor;
	}
}
