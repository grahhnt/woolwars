package com.grahhnt.WoolWars.MapEntities;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.entity.EntityType;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Map.MapEntity.EditorDescriptor.ConfigValue;
import com.grahhnt.Crossbow.Map.SinglePointMapEntity;
import com.grahhnt.Crossbow.Players.CrossbowTeam;
import com.grahhnt.WoolWars.NPC.KitSelectorTrait;

import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.trait.SkinTrait;

public class KitSelectorEntity extends SinglePointMapEntity {
	private NPC entity;
	private CrossbowTeam team;
	
	public KitSelectorEntity() {
		super("KIT_SELECTOR");
		
		entity = Crossbow.getInstance().entities.getRegistry().createNPC(EntityType.PLAYER, "kit selector");
		entity.getOrAddTrait(KitSelectorTrait.class);
		SkinTrait trait = entity.getOrAddTrait(SkinTrait.class);
		trait.setSkinName("grahhnt");
	}

	@Override
	public Map<String, Object> serialize() {
		LinkedHashMap<String, Object> serial = super.serializeBase();
		
		serial.put("team", team.getID());
		
		return serial;
	}

	@Override
	public void deserialize(Map<String, Object> data) {
		populateCoords(data);
		team = Crossbow.getInstance().players.getTeam((String) data.get("team"));
	}

	@Override
	public void spawn(World world) {
		entity.spawn(getLocation(world));
	}

	@Override
	public void destroy() {
		entity.despawn();
	}
	
	@Override
	public EditorDescriptor getEditorDescriptor() {
		EditorDescriptor descriptor = super.getEditorDescriptor();
		descriptor.addConfigValues(new ConfigValue.TeamValue("team", team, true));
		return descriptor;
	}
}
