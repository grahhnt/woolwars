package com.grahhnt.WoolWars.MapEntities;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.World;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Map.MapEntity.EditorDescriptor.ConfigValue;
import com.grahhnt.Crossbow.Map.SinglePointMapEntity;
import com.grahhnt.WoolWars.Powerups.Powerup;
import com.grahhnt.WoolWars.Powerups.PowerupInstance;

/**
 * Describes where a powerup can spawn
 * 
 * @author grant
 *
 */
public class PowerupEntity extends SinglePointMapEntity {
	private PowerupInstance instance;
	private String group;

	public PowerupEntity() {
		super("POWERUP");
	}

	@Override
	public Map<String, Object> serialize() {
		HashMap<String, Object> serial = super.serializeBase();
		
		serial.put("group", group);
		
		return serial;
	}

	@Override
	public void deserialize(Map<String, Object> data) {
		populateCoords(data);
		group = data.get("group").toString();
	}
	
	public void spawn(Powerup powerup) {
		instance = powerup.spawn(new Location(Crossbow.getInstance().maps.getActiveWorld(), x, y, z));
	}

	@Override
	public void spawn(World world) {
		
	}

	@Override
	public void destroy() {
		if(instance != null) {
			instance.destroy();
			instance = null;
		}
	}

	public String getGroup() {
		return group;
	}
	
	public PowerupInstance getEntity() {
		return instance;
	}
	
	@Override
	public EditorDescriptor getEditorDescriptor() {
		EditorDescriptor descriptor = super.getEditorDescriptor();
		descriptor.addConfigValues(new ConfigValue.StringValue("group", group, "1", false));
		return descriptor;
	}
}
