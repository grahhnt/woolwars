package com.grahhnt.WoolWars.MapEntities;

import java.util.Map;

import org.bukkit.World;

import com.grahhnt.Crossbow.Map.RegionMapEntity;

/**
 * Describes where the descructable blocks are
 * 
 * @author grant
 *
 */
public class WoolEntity extends RegionMapEntity {
	
	public WoolEntity() {
		super("WOOL");
	}

	@Override
	public Map<String, Object> serialize() {
		return super.serializeBase();
	}

	@Override
	public void deserialize(Map<String, Object> data) {
		populateRegionCoords(data);
	}

	@Override
	public void spawn(World world) {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}
}
