package com.grahhnt.WoolWars.MapEntities;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.World;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Map.MapEntity.EditorDescriptor.ConfigValue;
import com.grahhnt.Crossbow.Map.SinglePointMapEntity;
import com.grahhnt.Crossbow.Players.CrossbowTeam;

/**
 * Describes where a team spawns
 * 
 * @author grant
 *
 */
public class TeamSpawnEntity extends SinglePointMapEntity {
	private CrossbowTeam team;

	public TeamSpawnEntity() {
		super("TEAM_SPAWN");
	}

	@Override
	public Map<String, Object> serialize() {
		HashMap<String, Object> serial = super.serializeBase();
		
		serial.put("team", team.getID());
		
		return serial;
	}

	@Override
	public void deserialize(Map<String, Object> data) {
		populateCoords(data);
		team = Crossbow.getInstance().players.getTeam((String) data.get("team"));
	}

	@Override
	public void spawn(World world) {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	public CrossbowTeam getTeam() {
		return team;
	}
	
	@Override
	public EditorDescriptor getEditorDescriptor() {
		EditorDescriptor descriptor = super.getEditorDescriptor();
		descriptor.addConfigValues(new ConfigValue.TeamValue("team", team, true));
		return descriptor;
	}
}
