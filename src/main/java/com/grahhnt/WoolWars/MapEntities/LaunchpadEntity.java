package com.grahhnt.WoolWars.MapEntities;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.World;

import com.grahhnt.Crossbow.Map.MapEntity.EditorDescriptor.ConfigValue;
import com.grahhnt.Crossbow.Map.RegionMapEntity;

public class LaunchpadEntity extends RegionMapEntity {
	private double power;
	
	public LaunchpadEntity() {
		super("LAUNCHPAD");
	}

	@Override
	public Map<String, Object> serialize() {
		HashMap<String, Object> serial = super.serializeBase();
		
		serial.put("power", power);
		
		return serial;
	}

	@Override
	public void deserialize(Map<String, Object> data) {
		populateRegionCoords(data);
		power = Double.valueOf(data.get("power").toString());
	}

	@Override
	public void spawn(World world) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	public double getPower() {
		return power;
	}
	
	@Override
	public EditorDescriptor getEditorDescriptor() {
		EditorDescriptor descriptor = super.getEditorDescriptor();
		
		descriptor.addConfigValues(new ConfigValue.FloatValue("power", (float) power, 1f, true));
		
		return descriptor;
	}
}
