package com.grahhnt.WoolWars.GUIs;

import com.grahhnt.Crossbow.GUI.CrossbowGUI;
import com.grahhnt.WoolWars.GamePlayer;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.GameClasses.GameClass;

public class KitSelectorGUI extends CrossbowGUI {
	WoolWars ww;
	GamePlayer player;
	
	public KitSelectorGUI(GamePlayer player) {
		super("Kit Selector", 5);
		setOwner(player);
		this.player = player;

		ww = WoolWars.getInstance();
		
		setItem(getInventoryItem(ww.gameClasses.getClass("TANK")), 11);
		setItem(getInventoryItem(ww.gameClasses.getClass("ASSULT")), 13);
		setItem(getInventoryItem(ww.gameClasses.getClass("ARCHER")), 15);
		setItem(getInventoryItem(ww.gameClasses.getClass("SWORDSMAN")), 29);
		setItem(getInventoryItem(ww.gameClasses.getClass("GOLEM")), 31);
		setItem(getInventoryItem(ww.gameClasses.getClass("ENGINEER")), 33);
	}

	public CrossbowGUI.Item getInventoryItem(GameClass clazz) {
		CrossbowGUI.Item item = new CrossbowGUI.Item(clazz.getMaterial(), "&e" + clazz.getName(), 1);
		item.onClick(event -> {
			event.setCancelled(true);
			player.getPlayer().closeInventory();
			player.setGameClass(clazz);
		});
		return item;
	}
}
