package com.grahhnt.WoolWars;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;

import com.grahhnt.WoolWars.GameClasses.GameClass;

public class GamePlayer extends com.grahhnt.Crossbow.Players.GamePlayer {
	private GameClass pclass;

	public GamePlayer(Player player) {
		super(player);
		// default class to assult
		pclass = WoolWars.getInstance().gameClasses.getClass("ASSULT");
	}

	public void setGameClass(GameClass pclass) {
		this.pclass = pclass;
		giveClassItems();
	}

	public void giveClassItems() {
		getInventory().setKit(pclass.getKit());

		ItemStack[] armorStack = pclass.getArmor();
		for (int i = 0; i < pclass.getArmor().length; i++) {
			ItemStack armor = pclass.getArmor()[i];

			if (armor != null) {
				if (armor.getItemMeta() instanceof LeatherArmorMeta) {
					armorStack[i] = ((GameTeam) getTeam()).colorArmor(armor);
				}
			}
		}

		List<ItemStack> armorItems = Arrays.asList(armorStack);
		Collections.reverse(armorItems);
		getPlayer().getInventory().setArmorContents(armorItems.toArray(new ItemStack[4]));
		
		getPlayer().getActivePotionEffects().forEach(effect -> {
			getPlayer().removePotionEffect(effect.getType());
		});
		
		for(PotionEffect effect : pclass.getPotionEffect()) {
			getPlayer().addPotionEffect(new PotionEffect(effect.getType(), 99999, effect.getAmplifier()));
		}
	}

	public GameClass getGameClass() {
		return pclass;
	}
}
