package com.grahhnt.WoolWars;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Players.CrossbowTeam;

public class GameTeam extends CrossbowTeam {
	private int score = 0;
	/**
	 * Blocks currently placed in the center
	 */
	private int blocks = 0;
	private Material wool;
	private Color armorColor;

	public GameTeam(String id, String name, Material wool, String nameColor, Color armorColor) {
		super(id, name);
		this.wool = wool;
		setNameColor(nameColor);
		this.armorColor = armorColor;
	}

	public ItemStack colorArmor(ItemStack armor) {
		if (!(armor.getItemMeta() instanceof LeatherArmorMeta)) {
			throw new IllegalArgumentException();
		}

		LeatherArmorMeta itemMeta = (LeatherArmorMeta) armor.getItemMeta();
		itemMeta.setColor(armorColor);

		armor.setItemMeta(itemMeta);
		return armor;
	}

	public Material getWool() {
		return wool;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
		Crossbow.getInstance().scoreboard.updateAll();
	}

	public void addScore(int score) {
		setScore(this.score + score);
	}

	public int getBlocks() {
		return blocks;
	}

	public void setBlocks(int blocks) {
		this.blocks = blocks;
		Crossbow.getInstance().scoreboard.updateAll();
	}

	public String getBlocksPercentage() {
		String color = "&8";
		if (getBlocks() > 7) {
			color = "&c";
		} else if (getBlocks() > 4) {
			color = "&7";
		}

		DecimalFormat df = new DecimalFormat("0.0");

		return color + "(" + df.format(((double) getBlocks() / 9) * 100) + "%)";
	}

	public String getCircleScore() {
		String circle = "\u25CF";
		return "&a" + circle.repeat(score) + "&7" + circle
				.repeat((int) (Math.round((double) WoolWars.getInstance().getConfig().getInt("rounds", 5) / 2) - score));
	}

	@Override
	public void reset() {
		setScore(0);
		setBlocks(0);
	}
}
