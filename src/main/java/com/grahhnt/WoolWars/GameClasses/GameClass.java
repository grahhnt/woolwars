package com.grahhnt.WoolWars.GameClasses;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Kits.InventoryLayout;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.Keystones.Keystone;

public abstract class GameClass {
	public abstract String getID();

	public abstract String getName();

	public abstract String getDescription();

	public abstract Material getMaterial();

	/**
	 * Must be a length of 4 (helmet, chestplate, leggings, boots). Elements can be
	 * null.
	 * 
	 * @return
	 */
	public abstract ItemStack[] getArmor();

	public abstract InventoryLayout getKit();

	public abstract Keystone getKeystone();

	public abstract void register(WoolWars ww);
	
	public PotionEffect[] getPotionEffect() {
		return new PotionEffect[0];
	}

	InventoryLayout.TeamItem getWool(int slot) {
		return new InventoryLayout.TeamItem(slot)
				.addVariant(WoolWars.getInstance().game.players.getTeam("BLUE"),
						new CrossbowItem(Material.BLUE_WOOL, 64))
				.addVariant(WoolWars.getInstance().game.players.getTeam("RED"),
						new CrossbowItem(Material.RED_WOOL, 64));
	}
}
