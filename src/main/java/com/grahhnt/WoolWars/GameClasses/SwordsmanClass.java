package com.grahhnt.WoolWars.GameClasses;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Kits.InventoryLayout;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.Keystones.Keystone;

public class SwordsmanClass extends GameClass {

	@Override
	public String getID() {
		return "SWORDSMAN";
	}

	@Override
	public String getName() {
		return "Swordsman";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Material getMaterial() {
		return Material.STONE_SWORD;
	}

	@Override
	public ItemStack[] getArmor() {
		return new ItemStack[] { null, null, new ItemStack(Material.LEATHER_LEGGINGS),
				new ItemStack(Material.LEATHER_BOOTS) };
	}

	@Override
	public InventoryLayout getKit() {
		return WoolWars.getInstance().game.kits.getKit("SWORDSMAN");
	}

	@Override
	public Keystone getKeystone() {
		return WoolWars.getInstance().keystones.getKeystone("SWORDSMAN");
	}

	@Override
	public void register(WoolWars ww) {
		InventoryLayout swordsman = new InventoryLayout("SWORDSMAN", "Swordsman");
		swordsman.addItem(new InventoryLayout.Item(new CrossbowItem(Material.STONE_SWORD, 1), 0));
		swordsman.addItem(new InventoryLayout.Item(new CrossbowItem(Material.WOODEN_PICKAXE, 1), 2));
		swordsman.addItem(new InventoryLayout.Item(new CrossbowItem(Material.SHEARS, 1), 3));
		swordsman.addItem(getWool(4));
		swordsman.addItem(new InventoryLayout.Item(
				new CrossbowItem.Potion(1, true).potion(new PotionData(PotionType.INSTANT_HEAL, false, true)), 5));
		swordsman.addItem(new InventoryLayout.Item(getKeystone().getItem(), 8));
		ww.game.kits.registerKit(swordsman);
	}

}
