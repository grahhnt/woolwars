package com.grahhnt.WoolWars.GameClasses;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Kits.InventoryLayout;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.Keystones.Keystone;

public class EngineerClass extends GameClass {

	@Override
	public String getID() {
		return "ENGINEER";
	}

	@Override
	public String getName() {
		return "Engineer";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Material getMaterial() {
		return Material.REDSTONE_BLOCK;
	}

	@Override
	public ItemStack[] getArmor() {
		return new ItemStack[] { null, new ItemStack(Material.LEATHER_CHESTPLATE), null, null };
	}

	@Override
	public InventoryLayout getKit() {
		return WoolWars.getInstance().game.kits.getKit("ENGINEER");
	}

	@Override
	public Keystone getKeystone() {
		return WoolWars.getInstance().keystones.getKeystone("ENGINEER");
	}

	@Override
	public void register(WoolWars ww) {
		InventoryLayout engineer = new InventoryLayout("ENGINEER", "Engineer");
		engineer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.WOODEN_SWORD, 1), 0));
		engineer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.STONE_PICKAXE, 1), 2));
		engineer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.SHEARS, 1), 3));
		engineer.addItem(getWool(4));
		engineer.addItem(new InventoryLayout.Item(new CrossbowItem.Potion(2, true), 5));
		engineer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.ARROW, 4), 6));
		engineer.addItem(new InventoryLayout.Item(getKeystone().getItem(), 7));
		engineer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.BOW, 1), 8));
		ww.game.kits.registerKit(engineer);
	}

}
