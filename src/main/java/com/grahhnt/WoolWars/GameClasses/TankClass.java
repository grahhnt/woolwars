package com.grahhnt.WoolWars.GameClasses;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Kits.InventoryLayout;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.Keystones.Keystone;

public class TankClass extends GameClass {

	@Override
	public String getID() {
		return "TANK";
	}

	@Override
	public String getName() {
		return "Tank";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Material getMaterial() {
		return Material.IRON_BLOCK;
	}

	@Override
	public ItemStack[] getArmor() {
		return new ItemStack[] { null, new ItemStack(Material.LEATHER_CHESTPLATE),
				new ItemStack(Material.LEATHER_LEGGINGS), new ItemStack(Material.LEATHER_BOOTS) };
	}

	@Override
	public InventoryLayout getKit() {
		return WoolWars.getInstance().game.kits.getKit("TANK");
	}

	@Override
	public Keystone getKeystone() {
		return WoolWars.getInstance().keystones.getKeystone("TANK");
	}

	@Override
	public void register(WoolWars ww) {
		InventoryLayout tank = new InventoryLayout("TANK", "Tank");
		tank.addItem(new InventoryLayout.Item(new CrossbowItem(Material.WOODEN_SWORD, 1), 0));
		tank.addItem(new InventoryLayout.Item(new CrossbowItem(Material.WOODEN_PICKAXE, 1), 2));
		tank.addItem(new InventoryLayout.Item(new CrossbowItem(Material.SHEARS, 1), 3));
		tank.addItem(getWool(4));
		tank.addItem(new InventoryLayout.Item(getKeystone().getItem(), 8));
		ww.game.kits.registerKit(tank);
	}

}
