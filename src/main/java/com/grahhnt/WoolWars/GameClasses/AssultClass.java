package com.grahhnt.WoolWars.GameClasses;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Kits.InventoryLayout;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.Keystones.Keystone;

public class AssultClass extends GameClass {

	@Override
	public String getID() {
		return "ASSULT";
	}

	@Override
	public String getName() {
		return "Assult";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Material getMaterial() {
		return Material.SHEARS;
	}

	@Override
	public ItemStack[] getArmor() {
		return new ItemStack[] { null, new ItemStack(Material.LEATHER_CHESTPLATE, 1),
				new ItemStack(Material.LEATHER_LEGGINGS, 1), null };
	}

	@Override
	public InventoryLayout getKit() {
		return WoolWars.getInstance().game.kits.getKit("ASSULT");
	}

	@Override
	public Keystone getKeystone() {
		return WoolWars.getInstance().keystones.getKeystone("ASSULT");
	}

	@Override
	public void register(WoolWars ww) {
		InventoryLayout assult = new InventoryLayout("ASSULT", "Assult");
		assult.addItem(new InventoryLayout.Item(new CrossbowItem(Material.WOODEN_SWORD, 1), 0));
		assult.addItem(new InventoryLayout.Item(new CrossbowItem(Material.STONE_SHOVEL, 1), 1));
		assult.addItem(new InventoryLayout.Item(new CrossbowItem(Material.IRON_PICKAXE, 1), 2));
		assult.addItem(new InventoryLayout.Item(
				new CrossbowItem(Material.SHEARS, 1).enchantment(Enchantment.DIG_SPEED, 2), 3));
		assult.addItem(getWool(4));
		assult.addItem(new InventoryLayout.Item(
				new CrossbowItem.Potion(1, true).potion(new PotionData(PotionType.INSTANT_HEAL, false, true)), 5));
		assult.addItem(new InventoryLayout.Item(
				new CrossbowItem.Potion(1, true).potion(new PotionData(PotionType.INSTANT_DAMAGE, false, true)), 6));
		assult.addItem(new InventoryLayout.Item(getKeystone().getItem(), 8));
		ww.game.kits.registerKit(assult);
	}

}
