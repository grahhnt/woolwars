package com.grahhnt.WoolWars.GameClasses;

import java.util.HashMap;

import com.grahhnt.WoolWars.WoolWars;

public class GameClassManager {
	WoolWars ww;
	private HashMap<String, GameClass> classes = new HashMap<>();
	
	public GameClassManager(WoolWars ww) {
		this.ww = ww;
	}
	
	public void registerClass(GameClass clazz) {
		classes.put(clazz.getID(), clazz);
		clazz.register(ww);
	}
	
	public GameClass getClass(String id) {
		return classes.get(id);
	}
}
