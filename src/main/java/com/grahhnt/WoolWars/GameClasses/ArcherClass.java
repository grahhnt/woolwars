package com.grahhnt.WoolWars.GameClasses;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Kits.InventoryLayout;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.Keystones.Keystone;

public class ArcherClass extends GameClass {

	@Override
	public String getID() {
		return "ARCHER";
	}

	@Override
	public String getName() {
		return "Archer";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Material getMaterial() {
		return Material.BOW;
	}

	@Override
	public ItemStack[] getArmor() {
		return new ItemStack[] { null, null, null, new ItemStack(Material.LEATHER_BOOTS, 1) };
	}

	@Override
	public InventoryLayout getKit() {
		return WoolWars.getInstance().game.kits.getKit("ARCHER");
	}

	@Override
	public Keystone getKeystone() {
		return WoolWars.getInstance().keystones.getKeystone("ARCHER");
	}

	@Override
	public void register(WoolWars ww) {
		InventoryLayout archer = new InventoryLayout("ARCHER", "Archer");
		archer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.WOODEN_AXE, 1), 0));
		archer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.WOODEN_PICKAXE, 1), 2));
		archer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.SHEARS, 1), 3));
		archer.addItem(getWool(4));
		archer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.ARROW, 6), 6));
		archer.addItem(new InventoryLayout.Item(getKeystone().getItem(), 7));
		archer.addItem(new InventoryLayout.Item(new CrossbowItem(Material.BOW, 1), 8));
		ww.game.kits.registerKit(archer);
	}
	
	@Override
	public PotionEffect[] getPotionEffect() {
		return new PotionEffect[] { new PotionEffect(PotionEffectType.SPEED, 1, 1)};
	}

}
