package com.grahhnt.WoolWars.GameClasses;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Kits.InventoryLayout;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.Keystones.Keystone;

public class GolemClass extends GameClass {

	@Override
	public String getID() {
		return "GOLEM";
	}

	@Override
	public String getName() {
		return "Golem";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Material getMaterial() {
		return Material.GOLDEN_CHESTPLATE;
	}

	@Override
	public ItemStack[] getArmor() {
		return new ItemStack[] { null, null, null, new ItemStack(Material.GOLDEN_BOOTS) };
	}

	@Override
	public InventoryLayout getKit() {
		return WoolWars.getInstance().game.kits.getKit("GOLEM");
	}

	@Override
	public Keystone getKeystone() {
		return WoolWars.getInstance().keystones.getKeystone("GOLEM");
	}

	@Override
	public void register(WoolWars ww) {
		InventoryLayout golem = new InventoryLayout("GOLEM", "Golem");
		golem.addItem(new InventoryLayout.Item(new CrossbowItem(Material.STONE_SWORD, 1), 0));
		golem.addItem(getWool(4));
		golem.addItem(new InventoryLayout.Item(getKeystone().getItem(), 8));
		ww.game.kits.registerKit(golem);
	}

}
