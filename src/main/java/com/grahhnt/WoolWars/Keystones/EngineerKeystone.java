package com.grahhnt.WoolWars.Keystones;

import com.grahhnt.WoolWars.GamePlayer;

public class EngineerKeystone extends Keystone {
	public EngineerKeystone() {
		super("ENGINEER", "Hack", "Disable players from placing or breaking middle blocks for 3 seconds.");
	}

	@Override
	public void trigger(GamePlayer player) {
		player.sendMessage("hack");
	}
}
