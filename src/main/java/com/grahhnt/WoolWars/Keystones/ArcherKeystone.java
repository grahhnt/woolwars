package com.grahhnt.WoolWars.Keystones;

import com.grahhnt.WoolWars.GamePlayer;

public class ArcherKeystone extends Keystone {
	public ArcherKeystone() {
		super("ARCHER", "Step Back", "Push yourself back instantly!");
	}
	
	@Override
	public void trigger(GamePlayer player) {
		player.sendMessage("archer");
	}
}
