package com.grahhnt.WoolWars.Keystones;

import com.grahhnt.WoolWars.GamePlayer;

public class GolemKeystone extends Keystone {
	public GolemKeystone() {
		super("GOLEM", "Golden Shell", "Encase yourself in Goldem Armor for 5 seconds.");
	}
	
	@Override
	public void trigger(GamePlayer player) {
		player.sendMessage("golem");
	}
}
