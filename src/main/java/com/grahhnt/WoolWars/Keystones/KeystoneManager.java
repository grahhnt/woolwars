package com.grahhnt.WoolWars.Keystones;

import java.util.HashMap;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.grahhnt.WoolWars.GamePlayer;
import com.grahhnt.WoolWars.WoolWars;

public class KeystoneManager implements Listener {
	WoolWars ww;
	private HashMap<String, Keystone> keystones = new HashMap<>();

	public KeystoneManager(WoolWars ww) {
		this.ww = ww;
	}

	@EventHandler
	public void on(PlayerDropItemEvent event) {
		GamePlayer player = (GamePlayer) ww.game.players.getPlayer(event.getPlayer());
		if (player.getGameClass() == null)
			return;

		event.setCancelled(true);
		player.getGameClass().getKeystone().trigger(player);
	}

	public void registerKeystone(Keystone keystone) {
		keystones.put(keystone.id, keystone);
	}

	public Keystone getKeystone(String id) {
		return keystones.get(id);
	}
}
