package com.grahhnt.WoolWars.Keystones;

import com.grahhnt.WoolWars.GamePlayer;

public class SwordsmanKeystone extends Keystone {
	public SwordsmanKeystone() {
		super("SWORDSMAN", "Sprint", "Give yourself a speed boost for 3 seconds.");
	}
	
	public void trigger(GamePlayer player) {
		player.sendMessage("swordsman");
	}
}
