package com.grahhnt.WoolWars.Keystones;

import com.grahhnt.WoolWars.GamePlayer;

public class AssultKeystone extends Keystone {
	public AssultKeystone() {
		super("ASSULT", "Knockback TNT",
				"Place a TNT that doesn't deal damage, but deals massive knockback to enemies within 4 blocks.");
	}
	
	@Override
	public void trigger(GamePlayer player) {
		player.sendMessage("assult");
	}
}
