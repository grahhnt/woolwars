package com.grahhnt.WoolWars.Keystones;

import com.grahhnt.WoolWars.GamePlayer;

public class TankKeystone extends Keystone {
	public TankKeystone() {
		super("TANK", "${TANK KEYSTONE}", "idk this one gives you regen last i checked");
	}
	
	@Override
	public void trigger(GamePlayer player) {
		player.sendMessage("tank");
	}
}
