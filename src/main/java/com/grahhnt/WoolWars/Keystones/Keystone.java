package com.grahhnt.WoolWars.Keystones;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.event.block.Action;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.WoolWars.GamePlayer;
import com.grahhnt.WoolWars.WoolWars;

public abstract class Keystone {
	String id = null;
	String name = null;
	String description = null;

	public Keystone(String id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public CrossbowItem getItem() {
		WoolWars ww = WoolWars.getInstance();
		
		CrossbowItem item = new CrossbowItem(Material.BLAZE_POWDER, 1);
		item.setName("&fKeystone Ability: &6&l" + name.toUpperCase());

		ArrayList<String> lores = new ArrayList<String>();
		lores.add("&7" + description);
		lores.add(null);
		lores.add("&6Press &e&lQ &6or &e&lRight Click &6to activate");
		lores.add(null);
		lores.add("&7You can only use your keystone &c&lONCE &7per round.");
		
		item.setLore(lores.toArray(new String[lores.size()]));
		item.wrapLore(30);
		
		item.handleUse(e -> {
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				e.setCancelled(true);
				GamePlayer player = (GamePlayer) ww.game.players.getPlayer(e.getPlayer());
				player.getGameClass().getKeystone().trigger(player);
			}
		});
		
		return item;
	}
	
	public abstract void trigger(GamePlayer player);
}
