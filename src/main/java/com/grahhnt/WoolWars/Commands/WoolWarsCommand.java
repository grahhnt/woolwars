package com.grahhnt.WoolWars.Commands;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Material;

import com.grahhnt.Crossbow.Commands.CrossbowCommand;
import com.grahhnt.Crossbow.Map.MapEntity;
import com.grahhnt.Crossbow.Players.GamePlayer;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.MapEntities.PowerupEntity;
import com.grahhnt.WoolWars.MapEntities.TeamWallEntity;
import com.grahhnt.WoolWars.Powerups.Powerup;

public class WoolWarsCommand extends CrossbowCommand {
	WoolWars ww;

	public WoolWarsCommand(WoolWars ww) {
		super(ww.game, "woolwars");
		this.ww = ww;
	}

	@Command(name = "walls")
	public void walls(GamePlayer player, String[] args) {
		MapEntity[] entities = ww.game.maps.getMap().getEntities("TEAM_WALL");
		for (MapEntity entity : entities) {
			TeamWallEntity wall = (TeamWallEntity) entity;
			wall.setWall(player.getPlayer().getWorld(), Material.BARRIER);
		}
	}

	@Command(name = "kit")
	public void kit(GamePlayer player, String[] args) {
		player.getInventory().setKit(ww.game.kits.getKit("default"));
	}

	@Command(name = "class")
	public void gclass(GamePlayer player, String[] args) {
		((com.grahhnt.WoolWars.GamePlayer) player).setGameClass(ww.gameClasses.getClass(args[0]));
	}

	@Command(name = "kill")
	public void kill(GamePlayer player, String[] args) {
		player.kill(e -> {
			switch (args[0]) {
			case "cancel":
				e.setCancelled(true);
				break;
			case "drop":
				e.setShouldDropInventory(true);
				break;
			case "instant":
				e.setShouldInstantRespawn(true);
				break;
			}
		});
	}

	@Command(name = "rounds")
	public void rounds(GamePlayer player, String[] args) {
		player.sendMessage(ww.rounds.toString());
	}

	@Command(name = "spawn")
	public void spawn(GamePlayer player, String[] args) {
		if(args.length == 0) {
			player.sendMessage("&cMissing powerup! &7" + Arrays.toString(ww.powerups.getPowerups()));
			return;
		}
		
		Powerup powerup = ww.powerups.getPowerup(args[0]);
		if(powerup == null) {
			player.sendMessage("&cUnknown powerup &7" + args[0]);
			return;
		}
		
		List<PowerupEntity> entities = Arrays.stream(ww.game.maps.getMap().getEntities("POWERUP"))
				.map(e -> (PowerupEntity) e)
				.sorted(Comparator.comparingDouble(
						e -> e.getLocation(player.getPlayer().getWorld()).distance(player.getPlayer().getLocation())))
				.collect(Collectors.toList());
		
		entities.get(0).destroy();
		entities.get(0).spawn(powerup);
	}
}
