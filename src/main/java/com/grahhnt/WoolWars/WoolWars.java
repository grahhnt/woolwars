package com.grahhnt.WoolWars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.GameMetadata;
import com.grahhnt.Crossbow.PermissionsManager;
import com.grahhnt.Crossbow.Components.Functions;
import com.grahhnt.Crossbow.Events.GameEventHandler;
import com.grahhnt.Crossbow.Events.GameEventHandler.EventPriority;
import com.grahhnt.Crossbow.Events.GameListener;
import com.grahhnt.Crossbow.Events.Game.GameEndEvent;
import com.grahhnt.Crossbow.Events.Game.GameScoreboardUpdateEvent;
import com.grahhnt.Crossbow.Events.Game.GameStartEvent;
import com.grahhnt.Crossbow.Events.Game.GameStateEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDeathEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerJoinEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerRespawnEvent;
import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Items.MagicBedrock;
import com.grahhnt.Crossbow.Items.MapSelector;
import com.grahhnt.Crossbow.Kits.InventoryLayout;
import com.grahhnt.Crossbow.Map.MapEntity;
import com.grahhnt.Crossbow.Map.MapManager;
import com.grahhnt.Crossbow.Map.Config.MapConfiguration;
import com.grahhnt.Crossbow.Players.CrossbowTeam;
import com.grahhnt.WoolWars.Commands.WoolWarsCommand;
import com.grahhnt.WoolWars.GameClasses.ArcherClass;
import com.grahhnt.WoolWars.GameClasses.AssultClass;
import com.grahhnt.WoolWars.GameClasses.EngineerClass;
import com.grahhnt.WoolWars.GameClasses.GameClassManager;
import com.grahhnt.WoolWars.GameClasses.GolemClass;
import com.grahhnt.WoolWars.GameClasses.SwordsmanClass;
import com.grahhnt.WoolWars.GameClasses.TankClass;
import com.grahhnt.WoolWars.Keystones.ArcherKeystone;
import com.grahhnt.WoolWars.Keystones.AssultKeystone;
import com.grahhnt.WoolWars.Keystones.EngineerKeystone;
import com.grahhnt.WoolWars.Keystones.GolemKeystone;
import com.grahhnt.WoolWars.Keystones.KeystoneManager;
import com.grahhnt.WoolWars.Keystones.SwordsmanKeystone;
import com.grahhnt.WoolWars.Keystones.TankKeystone;
import com.grahhnt.WoolWars.MapEntities.KitSelectorEntity;
import com.grahhnt.WoolWars.MapEntities.LaunchpadEntity;
import com.grahhnt.WoolWars.MapEntities.PowerupEntity;
import com.grahhnt.WoolWars.MapEntities.TeamSpawnEntity;
import com.grahhnt.WoolWars.MapEntities.TeamWallEntity;
import com.grahhnt.WoolWars.MapEntities.WoolEntity;
import com.grahhnt.WoolWars.NPC.KitSelectorTrait;
import com.grahhnt.WoolWars.Powerups.Bow;
import com.grahhnt.WoolWars.Powerups.ChainmailChestplate;
import com.grahhnt.WoolWars.Powerups.ChainmailHelmet;
import com.grahhnt.WoolWars.Powerups.ChainmailLeggings;
import com.grahhnt.WoolWars.Powerups.InstantHealth;
import com.grahhnt.WoolWars.Powerups.IronBoots;
import com.grahhnt.WoolWars.Powerups.JumpBoost;
import com.grahhnt.WoolWars.Powerups.PowerupManager;
import com.grahhnt.WoolWars.Powerups.Speed;
import com.grahhnt.WoolWars.Powerups.StonePickaxe;
import com.grahhnt.WoolWars.Powerups.Strength;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.trait.TraitInfo;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class WoolWars extends JavaPlugin implements Listener, GameListener {
	private static WoolWars instance;
	public Crossbow game;

	public GameClassManager gameClasses;
	public KeystoneManager keystones;
	public PowerupManager powerups;

	public ArrayList<GameTeam> rounds = new ArrayList<>();

	public enum GameState {
		/**
		 * Players behind team barriers
		 */
		PREROUND,
		/**
		 * Round active
		 */
		ROUND,
		/**
		 * After a team scores, and before preround
		 */
		POSTROUND
	}

	// this builds on Crossbow#state for WoolWars-specific states
	GameState state = GameState.PREROUND;

	int midLockTime = 0;
	int roundTime = 0;
	BukkitTask roundTask = null;
	BukkitTask perTick = null;
	BukkitTask midLock = null;

	enum Team {
		RED, BLUE;

		public final GameTeam team;

		private Team() {
			team = Crossbow.getInstance().players.<GameTeam>getTeam(this.name());
		}
	}

	Function<Location, Location> getWorlded = l -> {
		l.setWorld(game.maps.getActiveWorld());
		return l;
	};

	@Override
	public void onEnable() {
		instance = this;

		game = new Crossbow.Builder(this).playerManager(GamePlayer.class, GameTeam.class, false).kitManager()
				.eventManager().scoreboardManager().mapManager().commandManager().npcs().permissionsManager("woolwars")
				.metadata(new GameMetadata.Builder().name("Wool Wars").players(2, 8).build()).build();

		game.setWinningFunction(Functions.teamWinner);

		CitizensAPI.getTraitFactory().registerTrait(TraitInfo.create(KitSelectorTrait.class));

		getServer().getPluginManager().registerEvents(this, this);

		game.players.registerTeam(new GameTeam("BLUE", "Blue", Material.BLUE_WOOL, "&9", Color.fromRGB(0, 0, 255)));
		game.players.registerTeam(new GameTeam("RED", "Red", Material.RED_WOOL, "&c", Color.fromRGB(255, 0, 0)));

		InventoryLayout kit = new InventoryLayout("default", "Default Kit");
		kit.addItem(new InventoryLayout.Item(game.items.get(MagicBedrock.class), 4));
		kit.addItem(new InventoryLayout.TeamItem(0)
				.addVariant(game.players.getTeam("BLUE"), new CrossbowItem(Material.BLUE_WOOL, 64))
				.addVariant(game.players.getTeam("RED"), new CrossbowItem(Material.RED_WOOL, 64)));
		game.kits.registerKit(kit);

		game.maps.registerEntity("POWERUP", PowerupEntity.class);
		game.maps.registerEntity("TEAM_SPAWN", TeamSpawnEntity.class);
		game.maps.registerEntity("TEAM_WALL", TeamWallEntity.class);
		game.maps.registerEntity("WOOL", WoolEntity.class);
		game.maps.registerEntity("KIT_SELECTOR", KitSelectorEntity.class);
		game.maps.registerEntity("LAUNCHPAD", LaunchpadEntity.class);
		game.events.registerEvents(this);
		game.commands.registerCommand("woolwars", new WoolWarsCommand(this));

		this.saveDefaultConfig();

		// load maps from config
		game.maps.loadMaps(getConfig());
		
		game.players.configReconnectTimeout(getConfig().getBoolean("reconnectTimeout.enable", true), getConfig().getInt("reconnectTimeout.timeout", 5));

		gameClasses = new GameClassManager(this);
		keystones = new KeystoneManager(this);

		keystones.registerKeystone(new TankKeystone());
		keystones.registerKeystone(new AssultKeystone());
		keystones.registerKeystone(new ArcherKeystone());
		keystones.registerKeystone(new SwordsmanKeystone());
		keystones.registerKeystone(new GolemKeystone());
		keystones.registerKeystone(new EngineerKeystone());

		gameClasses.registerClass(new TankClass());
		gameClasses.registerClass(new AssultClass());
		gameClasses.registerClass(new ArcherClass());
		gameClasses.registerClass(new SwordsmanClass());
		gameClasses.registerClass(new GolemClass());
		gameClasses.registerClass(new EngineerClass());

		powerups = new PowerupManager(this);

		powerups.register(new Bow());
		powerups.register(new ChainmailChestplate());
		powerups.register(new ChainmailHelmet());
		powerups.register(new ChainmailLeggings());
		powerups.register(new InstantHealth());
		powerups.register(new IronBoots());
		powerups.register(new JumpBoost());
		powerups.register(new Speed());
		powerups.register(new StonePickaxe());
		powerups.register(new Strength());

		getServer().getPluginManager().registerEvents(keystones, this);
		getServer().getPluginManager().registerEvents(powerups, this);

		game.events.catchup();

		perTick = new BukkitRunnable() {
			@Override
			public void run() {
				if (game.maps.getMap() != null)
					Arrays.stream(game.maps.getMap().getEntities("LAUNCHPAD")).map(a -> (LaunchpadEntity) a)
							.forEach(launchpad -> {
								CrossbowUtils.areaBlockStream(launchpad).forEach(block -> {
									game.maps.getActiveWorld().spawnParticle(Particle.VILLAGER_HAPPY,
											getWorlded.apply(block), 1, 0.5, 0.2, 0.5);
								});
							});
			}
		}.runTaskTimer(this, 0L, 1L);
	}

	@Override
	public void onDisable() {
		game.shutdown();
		CitizensAPI.getTraitFactory().deregisterTrait(TraitInfo.create(KitSelectorTrait.class));

		if (roundTask != null) {
			roundTask.cancel();
			roundTask = null;
		}

		if (perTick != null) {
			perTick.cancel();
			perTick = null;
		}

		if (midLock != null) {
			midLock.cancel();
			midLock = null;
		}
	}

	public static WoolWars getInstance() {
		return instance;
	}

	@GameEventHandler
	public void on(GameScoreboardUpdateEvent event) {
		ArrayList<String> items = new ArrayList<>(event.getItems());

		// if Crossbow is in active-game, use WoolWars-specific game states and render
		// them
		if (game.getGameState() == Crossbow.GameState.GAME) {
			switch (state) {
			case PREROUND:
				items.add("pick a class!");
				break;
			case ROUND:
				items.add("Time Left: &a" + String.format("%02d:%02d", (roundTime % 3600) / 60, roundTime % 60));
				break;
			case POSTROUND:
				break;
			}
			items.add(null);

			for (GameTeam team : game.players.<GameTeam>getTeams()) {
				items.add(team.getNameColor() + "&l" + team.getName() + " &e" + team.getCircleScore() + " "
						+ team.getBlocksPercentage());
			}

			String progress = "";
			for (int i = 0; i < getConfig().getInt("rounds", 5); i++) {
				String color = "&7";
				if (rounds.size() > i && rounds.get(i) != null) {
					color = rounds.get(i).getNameColor();
				} else if (rounds.size() > i && rounds.get(i) == null) {
					color = "&8";
				} else if (i == rounds.size()) {
					// highlight current round
					color = "&e";
				}
				progress += color + "-";
			}
			items.add(progress);
		}

		if (getConfig().contains("scoreboard.footer") && getConfig().getList("scoreboard.footer").size() > 0) {
			items.add(null);
			for (String item : getConfig().getList("scoreboard.footer").stream().map(l -> (String) l)
					.collect(Collectors.toList())) {
				items.add(item);
			}
		}

		event.setItems(items);
	}

	@GameEventHandler
	public void on(GameStartEvent event) {
		boolean canStart = true;

		for (CrossbowTeam team : game.players.getTeams()) {
			if (team.getPlayers().size() < 1) {
				canStart = false;
			}
		}

		if (!canStart) {
			event.setCancelled(true, "Not enough players on each team");
		}
	}

	@GameEventHandler(priority = EventPriority.HIGH)
	public void on(GamePlayerJoinEvent event) {

		if (!event.isRejoin()) {
			CrossbowTeam team = game.players.determineTeam(event.getPlayer());
			if (team == null) {
				event.getPlayer().sendMessage("&cFailed to assign you a team &7There isn't any space on any team");
			} else {
				team.addPlayer(event.getPlayer());
				event.getPlayer().sendMessage("&aYou have been added to team &f" + team.getName());
			}
		}

		if (game.getGameState() == Crossbow.GameState.LOBBY || game.getGameState() == Crossbow.GameState.PREGAME) {
			event.getPlayer().getInventory().clear();
			if (event.getPlayer().hasPermission(PermissionsManager.node(MapManager.Permissions.MAP_CHANGE))) {
				event.getPlayer().getInventory().give(game.items.get(MapSelector.class));
			}
		}
	}

	private void placeRandomCenter() {
		List<Material> blockTypes = List.of(Material.WHITE_WOOL, Material.SNOW_BLOCK, Material.QUARTZ_BLOCK);

		CrossbowUtils.areaBlockStream((WoolEntity) game.maps.getMap().getEntities("WOOL")[0]).forEach(location -> {
			location = getWorlded.apply(location);
			location.getBlock().setType(CrossbowUtils.randomElement(blockTypes));
		});
	}

	public void setState(GameState state) {
		this.state = state;
		CrossbowUtils.clearTasks();

		switch (state) {
		case PREROUND:
			game.players.<GameTeam>getTeams().stream().forEach(team -> {
				team.setBlocks(0);
			});

			List<GamePlayer> players = game.players.connected().stream().map(a -> (GamePlayer) a)
					.collect(Collectors.toList());
			for (GamePlayer player : players) {
				player.getPlayer().setGameMode(GameMode.SURVIVAL);

				// get TEAM_SPAWN entities -> filter to get team's TEAM_SPAWN, then get
				// shouldn't have to check for null as config should already have it specified
				MapEntity entity = Arrays.stream(game.maps.getMap().getEntities("TEAM_SPAWN"))
						.filter(e -> ((TeamSpawnEntity) e).getTeam().getID().equals(player.getTeam().getID()))
						.findFirst().get();
				Location loc = ((TeamSpawnEntity) entity).getLocation(player.getPlayer().getWorld());
				player.getPlayer().teleport(loc);
				player.giveClassItems();
				player.heal(20);
			}

			for (MapEntity entity : game.maps.getMap().getEntities("TEAM_WALL")) {
				TeamWallEntity wall = (TeamWallEntity) entity;
				wall.setWall(game.maps.getActiveWorld(), Material.BARRIER);
			}

			for (MapEntity entity : game.maps.getMap().getEntities("KIT_SELECTOR")) {
				KitSelectorEntity npc = (KitSelectorEntity) entity;
				npc.spawn(game.maps.getActiveWorld());
			}

			placeRandomCenter();
			powerups.spawn();

			CrossbowUtils.countdown(10, second -> {
				game.players.broadcast("&eRound starting in &c" + second + " seconds...");
				game.players.broadcast(Sound.BLOCK_NOTE_BLOCK_BASEDRUM);
			}, s -> {
				setState(GameState.ROUND);
			});
			break;
		case ROUND:
			midLockTime = getConfig().getInt("lockMidForSeconds", 5);
			midLock = new BukkitRunnable() {
				@Override
				public void run() {
					if (midLockTime - 1 < 0) {
						midLock.cancel();
						midLock = null;
					}

					midLockTime--;
					game.players.connected().stream().forEach(player -> {
						player.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR,
								new ComponentBuilder(midLockTime + " seconds until mid-unlock...")
										.color(ChatColor.YELLOW).create());
					});
				}
			}.runTaskTimer(this, 0L, 20L);

			// possibly a config variable?
			roundTime = 60;
			for (MapEntity entity : game.maps.getMap().getEntities("TEAM_WALL")) {
				TeamWallEntity wall = (TeamWallEntity) entity;
				wall.setWall(game.maps.getActiveWorld(), Material.AIR);
			}

			for (MapEntity entity : game.maps.getMap().getEntities("KIT_SELECTOR")) {
				KitSelectorEntity npc = (KitSelectorEntity) entity;
				npc.destroy();
			}

			roundTask = new BukkitRunnable() {
				@Override
				public void run() {
					if (roundTime - 1 < 0) {
						roundTask.cancel();
						roundTask = null;

						// updates blocks placed by teams
						getRoundWinner();

						List<GameTeam> teams = game.players.<GameTeam>getTeams().stream()
								.sorted(Comparator.comparing(GameTeam::getBlocks, Integer::compare).reversed())
								.collect(Collectors.toList());

						if (teams.get(0).getBlocks() != teams.get(1).getBlocks() && teams.get(0).getBlocks() > 0) {
							// the first and second place aren't the same; so first team has placed more
							teams.get(0).addScore(1);
							announceRoundWinner(teams.get(0));

							GameTeam winner = determineWinner();
							if (winner != null) {
								winner.setWinner(true);
								game.setGameState(Crossbow.GameState.POSTGAME);
							} else {
								// if no team has won, continue to next round
								setState(GameState.POSTROUND);
							}
						} else {
							announceRoundWinner(null);
							setState(GameState.POSTROUND);
						}
						return;
					}

					if (roundTime - 1 > 10) {
						getRoundWinner();

						List<GameTeam> teams = game.players.<GameTeam>getTeams().stream().sorted(Comparator
								.<GameTeam, Integer>comparing(team -> team.getAlivePlayers().size(), Integer::compare)
								.reversed()).collect(Collectors.toList());

						// if second team has no alive players, set countdown to 10
						if (teams.get(1).getAlivePlayers().size() == 0) {
							roundTime = 11;
						}
					}

					roundTime--;

					switch (roundTime) {
					case 10:
					case 5:
					case 4:
					case 3:
					case 2:
					case 1:
						game.players.broadcast("&c" + roundTime, "&eseconds left");
						game.players.broadcast(Sound.BLOCK_NOTE_BLOCK_BASEDRUM);
					}

					game.scoreboard.updateAll();
				}
			}.runTaskTimer(this, 0L, 20L);
			break;
		case POSTROUND:
			if (roundTask != null) {
				roundTask.cancel();
				roundTask = null;
			}

			if (rounds.size() + 1 > getConfig().getInt("rounds", 5)) {
				game.setGameState(Crossbow.GameState.POSTGAME);
				return;
			}

			game.players.all().stream().forEach(p -> {
				p.getPlayer().setGameMode(GameMode.SPECTATOR);
			});

			CrossbowUtils.countdown(5, second -> {
				game.players.all().stream().forEach(p -> {
					p.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR,
							new ComponentBuilder(second + " seconds...").color(ChatColor.YELLOW).create());
				});
			}, s -> {
				setState(GameState.PREROUND);
			});
			break;
		}

		game.scoreboard.updateAll();
	}

	private void announceRoundWinner(GameTeam team) {
		rounds.add(team);
		for (GamePlayer player : game.players.<GamePlayer>all()) {
			if (team == null) {
				player.sendTitle(CrossbowUtils.format("&cNo one"), CrossbowUtils.format("&ewon this round"));
			} else {
				player.sendTitle(team.getName(), CrossbowUtils.format("&ewon this round!"));
			}
		}
	}

	private int getBlocksPlacedByTeam(GameTeam team) {
		WoolEntity wool = (WoolEntity) game.maps.getMap().getEntities("WOOL")[0];
		return (int) CrossbowUtils.areaBlockStream(wool).filter(l -> {
			Material type = getWorlded.apply(l).getBlock().getType();

			if (team == null) {
				boolean valid = true;
				for (GameTeam t : game.players.<GameTeam>getTeams()) {
					if (t.getWool() == type)
						valid = false;
				}
				return valid;
			}

			return type == team.getWool();
		}).count();
	}

	private GameTeam getRoundWinner() {
		for (GameTeam team : game.players.<GameTeam>getTeams()) {
			team.setBlocks(getBlocksPlacedByTeam(team));
		}
		int none = getBlocksPlacedByTeam(null);
		if (none > 0)
			return null;

		int allBlocks = none
				+ game.players.<GameTeam>getTeams().stream().map(t -> t.getBlocks()).reduce(Integer::sum).orElse(0);

		return game.players.<GameTeam>getTeams().stream().filter(t -> (t.getBlocks() - allBlocks) == 0).findFirst()
				.get();
	}

	private GameTeam determineWinner() {
		return game.players.<GameTeam>getTeams().stream()
				.filter(t -> t.getScore() >= Math.round((double) getConfig().getInt("rounds", 5) / 2)).findFirst()
				.orElse(null);
	}

	@EventHandler
	public void on(InventoryClickEvent event) {
		if (event.getClickedInventory() != null && event.getClickedInventory().getType() == InventoryType.PLAYER
				&& event.getSlotType() == SlotType.ARMOR) {
			event.setResult(Result.DENY);
		}
	}

	@EventHandler
	public void on(BlockPlaceEvent event) {
		if (game.maps.getMap() == null || state != GameState.ROUND) {
			event.setCancelled(true);
			return;
		}

		WoolEntity wool = (WoolEntity) game.maps.getMap().getEntities("WOOL")[0];
		if (!wool.isLocationWithin(event.getBlock().getLocation())) {
			event.setCancelled(true);
			event.setBuild(false);
			game.players.getPlayer(event.getPlayer()).sendMessage("&cYou cannot place here");
		} else {
			GameTeam winningTeam = getRoundWinner();

			if (winningTeam != null) {
				winningTeam.addScore(1);
				announceRoundWinner(winningTeam);

				GameTeam winner = determineWinner();
				if (winner != null) {
					winner.setWinner(true);
					game.setGameState(Crossbow.GameState.POSTGAME);
				} else {
					// if no team has won, continue to next round
					setState(GameState.POSTROUND);
				}
			}
		}
	}

	@EventHandler
	public void on(BlockBreakEvent event) {
		if (event.getPlayer().getGameMode() == GameMode.CREATIVE)
			return;

		event.setDropItems(false);
		if (game.maps.getMap() == null || state != GameState.ROUND) {
			event.setCancelled(true);
			return;
		}

		GamePlayer player = game.players.getPlayer(event.getPlayer());

		WoolEntity wool = (WoolEntity) game.maps.getMap().getEntities("WOOL")[0];
		if (!wool.isLocationWithin(event.getBlock().getLocation())) {
			event.setCancelled(true);
			player.sendMessage("&cYou cannot break here");
		} else {
			if (midLockTime > 0) {
				event.setCancelled(true);
				player.sendMessage("&cMid is still locked");
			}
		}
	}

	@EventHandler
	public void on(PlayerMoveEvent event) {
		if (game.maps.getMap() == null && !MapConfiguration.hasInstance())
			return;

		List<LaunchpadEntity> intersecting = Arrays.stream(game.maps.getMap().getEntities("LAUNCHPAD"))
				.map(a -> (LaunchpadEntity) a)
				.filter(launchpad -> launchpad.isLocationWithin(event.getPlayer().getLocation()))
				.collect(Collectors.toList());

		GamePlayer player = game.players.getPlayer(event.getPlayer());

		for (LaunchpadEntity launchpad : intersecting) {
			player.getPlayer().setVelocity(new Vector(0, launchpad.getPower(), 0));
		}
	}

	@EventHandler
	public void on(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}

	@GameEventHandler
	public void on(GameStateEvent event) {
		switch (event.getNewState()) {
		case GAME:
			// change all players' gamemodes to survival to allow block breaking & placing
			// as Spigot's API doesn't have support for adding CanBreak & CanPlace natively
			//
			// might add that as a utility/library or just depend on a third party library
			ArrayList<GamePlayer> players = game.players.all();
			for (GamePlayer player : players) {
				player.getPlayer().setGameMode(GameMode.SURVIVAL);
			}

			setState(GameState.PREROUND);
			break;
		case POSTGAME:
			CrossbowUtils.clearTasks();
			if (roundTask != null) {
				roundTask.cancel();
				roundTask = null;
			}
			break;
		}
	}

	@GameEventHandler
	public void onDeath(GamePlayerDeathEvent event) {
		event.setShouldInstantRespawn(true);
		event.setShouldDropInventory(false);
	}

	@GameEventHandler
	public void onRespawn(GamePlayerRespawnEvent event) {
		if (game.getGameState() == Crossbow.GameState.GAME) {
			event.setCancelled(true);
			event.getPlayer().getPlayer().setGameMode(GameMode.SPECTATOR);
		}
	}

	@GameEventHandler
	public void cleanupGame(GameEndEvent event) {
		CrossbowUtils.clearTasks();
		game.players.<GameTeam>getTeam("RED").reset();
		game.players.<GameTeam>getTeam("BLUE").reset();
		rounds.clear();
	}
}
