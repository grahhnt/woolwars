package com.grahhnt.WoolWars.Powerups;

import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Items.VanillaCrossbowItem;
import com.grahhnt.WoolWars.GamePlayer;

public class StonePickaxe extends Powerup {
	public StonePickaxe() {
		super("stone_pickaxe");
	}

	@Override
	public String getName() {
		return "Stone Pickaxe";
	}

	@Override
	public void collect(GamePlayer player) {
		CrossbowUtils.upgradeItems(player.getPlayer().getInventory(), new CrossbowItem(Material.STONE_PICKAXE, 1));
	}

	@Override
	public DisplayItem getDisplayItem() {
		return new DisplayItem(new ItemStack(Material.STONE_PICKAXE), EquipmentSlot.HAND);
	}

	@Override
	public boolean canCollect(GamePlayer player) {
		boolean canCollect = false;

		for (ItemStack item : player.getPlayer().getInventory().getContents()) {
			if (item == null)
				continue;
			
			// if player has a better item then a stone pickaxe, it can be upgraded
			if(VanillaCrossbowItem.get(item.getType()).isBetter(VanillaCrossbowItem.get(Material.STONE_PICKAXE))) {
				canCollect = true;
			}
		}

		return !canCollect;
	}
}
