package com.grahhnt.WoolWars.Powerups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.Map.MapEntity;
import com.grahhnt.WoolWars.GamePlayer;
import com.grahhnt.WoolWars.WoolWars;
import com.grahhnt.WoolWars.MapEntities.PowerupEntity;

public class PowerupManager implements Listener {
	WoolWars game;
	HashMap<String, Powerup> powerups = new HashMap<>();

	public PowerupManager(WoolWars game) {
		this.game = game;
	}

	public void register(Powerup powerup) {
		powerups.put(powerup.id, powerup);
	}

	public Powerup getPowerup(String id) {
		return powerups.get(id);
	}

	public String[] getPowerups() {
		return powerups.keySet().toArray(String[]::new);
	}

	public void spawn() {
		HashMap<String, ArrayList<PowerupEntity>> grouped = new HashMap<>();

		for (MapEntity entity : game.game.maps.getMap().getEntities("POWERUP")) {
			PowerupEntity powerup = (PowerupEntity) entity;
			// despawn old powerup
			powerup.destroy();

			if (!grouped.containsKey(powerup.getGroup()))
				grouped.put(powerup.getGroup(), new ArrayList<>());

			grouped.get(powerup.getGroup()).add(powerup);
		}

		for (String group : grouped.keySet()) {
			Powerup powerup = CrossbowUtils.randomElement(new ArrayList<Powerup>(powerups.values()));

			for (PowerupEntity entity : grouped.get(group)) {
				entity.spawn(powerup);
			}
		}
	}

	@EventHandler
	public void on(PlayerMoveEvent event) {
		GamePlayer player = game.game.players.getPlayer(event.getPlayer());
		List<PowerupInstance> intersecting = powerups.values().stream().map(p -> p.isIntersecting(event.getTo()))
				.filter(p -> p.size() > 0).flatMap(List::stream).collect(Collectors.toList());

		for (PowerupInstance instance : intersecting) {
			if (!instance.powerup.canCollect(player)) {
				player.sendMessage("&cCan't collect this powerup.");
				continue;
			}
			
			player.getPlayer().playSound(event.getTo(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 1);
			player.sendMessage("&eCollected &c" + instance.powerup.getName());

			instance.powerup.collect(player);
			instance.destroy();
		}
	}
}
