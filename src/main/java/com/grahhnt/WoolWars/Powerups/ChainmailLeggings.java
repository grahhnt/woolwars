package com.grahhnt.WoolWars.Powerups;

import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Items.VanillaCrossbowItem;
import com.grahhnt.WoolWars.GamePlayer;

public class ChainmailLeggings extends Powerup {
	public ChainmailLeggings() {
		super("chainmail_leggings");
	}

	@Override
	public String getName() {
		return "Chainmail Leggings";
	}

	@Override
	public void collect(GamePlayer player) {
		player.getInventory().set(EquipmentSlot.LEGS, new CrossbowItem(Material.CHAINMAIL_LEGGINGS, 1));
	}

	@Override
	public DisplayItem getDisplayItem() {
		return new DisplayItem(new ItemStack(Material.CHAINMAIL_LEGGINGS), EquipmentSlot.LEGS);
	}

	@Override
	public boolean canCollect(GamePlayer player) {
		return VanillaCrossbowItem.get(Material.CHAINMAIL_LEGGINGS)
				.isBetter(VanillaCrossbowItem.get(player.getPlayer().getInventory().getItem(EquipmentSlot.LEGS)));
	}
}
