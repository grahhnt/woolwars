package com.grahhnt.WoolWars.Powerups;

import org.bukkit.entity.ArmorStand;

public class PowerupInstance {
	Powerup powerup;
	ArmorStand armorStand;

	PowerupInstance(Powerup powerup, ArmorStand armorStand) {
		this.powerup = powerup;
		this.armorStand = armorStand;
	}

	public void destroy() {
		if (armorStand != null) {
			armorStand.remove();
			armorStand = null;
		}

		powerup.instances.remove(this);
	}

	public ArmorStand getArmorStand() {
		return armorStand;
	}
}
