package com.grahhnt.WoolWars.Powerups;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.WoolWars.GamePlayer;

public class InstantHealth extends Powerup {
	public InstantHealth() {
		super("instant_health");
	}

	@Override
	public String getName() {
		return "Instant Health";
	}

	@Override
	public void collect(GamePlayer player) {
		player.heal(4);
		player.sendMessage("&c+2 hearts");
	}

	@Override
	public DisplayItem getDisplayItem() {
		return new DisplayItem(new ItemStack(Material.SPLASH_POTION), EquipmentSlot.HAND);
	}

	@Override
	public boolean canCollect(GamePlayer player) {
		return player.getHealth() < player.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
	}
}
