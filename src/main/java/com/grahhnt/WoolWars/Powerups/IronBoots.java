package com.grahhnt.WoolWars.Powerups;

import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Items.VanillaCrossbowItem;
import com.grahhnt.WoolWars.GamePlayer;

public class IronBoots extends Powerup {
	public IronBoots() {
		super("iron_boots");
	}

	@Override
	public String getName() {
		return "Iron Boots";
	}

	@Override
	public void collect(GamePlayer player) {
		player.getInventory().set(EquipmentSlot.FEET, new CrossbowItem(Material.IRON_BOOTS, 1));
	}

	@Override
	public DisplayItem getDisplayItem() {
		return new DisplayItem(new ItemStack(Material.IRON_BOOTS), EquipmentSlot.FEET);
	}

	@Override
	public boolean canCollect(GamePlayer player) {
		return VanillaCrossbowItem.get(Material.IRON_BOOTS)
				.isBetter(VanillaCrossbowItem.get(player.getPlayer().getInventory().getItem(EquipmentSlot.FEET)));
	}
}
