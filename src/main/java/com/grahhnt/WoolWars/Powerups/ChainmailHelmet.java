package com.grahhnt.WoolWars.Powerups;

import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Items.VanillaCrossbowItem;
import com.grahhnt.WoolWars.GamePlayer;

public class ChainmailHelmet extends Powerup {
	public ChainmailHelmet() {
		super("chainmail_helmet");
	}

	@Override
	public String getName() {
		return "Chainmail Helmet";
	}

	@Override
	public void collect(GamePlayer player) {
		player.getInventory().set(EquipmentSlot.HEAD, new CrossbowItem(Material.CHAINMAIL_HELMET, 1));
	}

	@Override
	public DisplayItem getDisplayItem() {
		return new DisplayItem(new ItemStack(Material.CHAINMAIL_HELMET), EquipmentSlot.HEAD);
	}

	@Override
	public boolean canCollect(GamePlayer player) {
		return VanillaCrossbowItem.get(Material.CHAINMAIL_HELMET)
				.isBetter(VanillaCrossbowItem.get(player.getPlayer().getInventory().getItem(EquipmentSlot.HEAD)));
	}
}
