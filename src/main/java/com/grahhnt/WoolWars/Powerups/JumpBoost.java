package com.grahhnt.WoolWars.Powerups;

import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.grahhnt.WoolWars.GamePlayer;

public class JumpBoost extends Powerup {
	public JumpBoost() {
		super("jump_boost");
	}
	
	@Override
	public String getName() {
		return "Jump Boost";
	}

	@Override
	public void collect(GamePlayer player) {
		player.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 5 * 20, 2));
	}

	@Override
	public DisplayItem getDisplayItem() {
		return new DisplayItem(new ItemStack(Material.SPLASH_POTION), EquipmentSlot.HAND);
	}
	
	@Override
	public boolean canCollect(GamePlayer player) {
		return !player.getPlayer().hasPotionEffect(PotionEffectType.JUMP);
	}
}
