package com.grahhnt.WoolWars.Powerups;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.WoolWars.GamePlayer;

public abstract class Powerup {
	String id;
	ArrayList<PowerupInstance> instances = new ArrayList<>();

	protected Powerup(String id) {
		this.id = id;
	}
	
	public abstract String getName();

	public abstract void collect(GamePlayer player);
	
	public abstract boolean canCollect(GamePlayer player);

	public PowerupInstance spawn(Location location) {
		ArmorStand armorStand = location.getWorld().spawn(location, ArmorStand.class);
		armorStand.getEquipment().setItem(getDisplayItem().slot, getDisplayItem().item);
		armorStand.setCustomNameVisible(true);
		armorStand.setCustomName(getName());
		armorStand.setInvisible(true);
		armorStand.setInvulnerable(true);

		PowerupInstance instance = new PowerupInstance(this, armorStand);
		instances.add(instance);
		return instance;
	}

	public void destroy() {
		for (PowerupInstance instance : instances) {
			instance.destroy();
		}
		instances.clear();
	}

	public List<PowerupInstance> isIntersecting(Location location) {
		return instances.stream()
				.filter(i -> CrossbowUtils.isLocationInArea(i.armorStand.getLocation().clone().subtract(0.5, 0.5, 0.5),
						i.armorStand.getLocation().clone().add(0.5, 2.5, 0.5), location))
				.collect(Collectors.toList());
	}

	public abstract DisplayItem getDisplayItem();

	static class DisplayItem {
		public ItemStack item;
		public EquipmentSlot slot;

		public DisplayItem(ItemStack item, EquipmentSlot slot) {
			this.item = item;
			this.slot = slot;
		}
	}
}
