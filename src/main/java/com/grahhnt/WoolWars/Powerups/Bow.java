package com.grahhnt.WoolWars.Powerups;

import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.WoolWars.GamePlayer;

public class Bow extends Powerup {
	public Bow() {
		super("bow");
	}

	@Override
	public String getName() {
		return "Bow";
	}

	@Override
	public void collect(GamePlayer player) {
		player.getInventory().give(new CrossbowItem(Material.BOW, 1));
		player.getInventory().give(new CrossbowItem(Material.ARROW, 2));
	}

	@Override
	public DisplayItem getDisplayItem() {
		return new DisplayItem(new ItemStack(Material.BOW, 1), EquipmentSlot.HAND);
	}

	@Override
	public boolean canCollect(GamePlayer player) {
		// you can always collect arrows, and a bow
		return true;
	}
}
