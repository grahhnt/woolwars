package com.grahhnt.WoolWars.Powerups;

import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Items.VanillaCrossbowItem;
import com.grahhnt.WoolWars.GamePlayer;

public class ChainmailChestplate extends Powerup {
	public ChainmailChestplate() {
		super("chainmail_chestplate");
	}

	@Override
	public String getName() {
		return "Chainmail Chestplate";
	}

	@Override
	public void collect(GamePlayer player) {
		player.getInventory().set(EquipmentSlot.CHEST, new CrossbowItem(Material.CHAINMAIL_CHESTPLATE, 1));
	}

	@Override
	public DisplayItem getDisplayItem() {
		return new DisplayItem(new ItemStack(Material.CHAINMAIL_CHESTPLATE), EquipmentSlot.CHEST);
	}

	@Override
	public boolean canCollect(GamePlayer player) {
		return VanillaCrossbowItem.get(Material.CHAINMAIL_CHESTPLATE)
				.isBetter(VanillaCrossbowItem.get(player.getPlayer().getInventory().getItem(EquipmentSlot.CHEST)));
	}
}
